package cn.lastcc.pwdmanage.core;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import cn.lastcc.pwdmanage.entity.Info;

/**
 * 重写了 Android Application 对象，主要用于自定义初始化动作
 * 
 * @author Added by baidongyang 2013-6-23
 * @version 13.6.23.1
 *
 */
public class CustomApp extends Application {

    private String userPwd;
    private int versionCode = 0;
    private String versionName = "";

    /**
     * 获取全局变量：当前用户登陆密码
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public String getUserPwd() {
        return userPwd;
    }

    /**
     * 设置全局变量：当前用户登陆密码
     * @param userPwd
     * @author Added by baidongyang 2013-6-23
     */
    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    /**
     * 获取全局变量：代码版本号
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public int getVersionCode() {
        return versionCode;
    }

    /**
     * 设置全局变量：代码版本号
     * @param versionCode
     * @author Added by baidongyang 2013-6-23
     */
    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    /**
     * 获取全局变量：显示版本号
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public String getVersionName() {
        return versionName;
    }

    /**
     * 设置全局变量：显示版本号
     * @param versionName
     * @author Added by baidongyang 2013-6-23
     */
    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // 初始化全局变量
        Info info = null;
        try {
            info = SerialUtil.getInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null != info) {
            this.setUserPwd(info.getUserPassword());
        }
        PackageInfo packageInfo = null;;
        try {
            packageInfo = this.getPackageManager().getPackageInfo(super.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("debug", String.valueOf(packageInfo));
        // 获得版本号
        if (null != packageInfo) {
            this.versionCode = packageInfo.versionCode;
            this.versionName = packageInfo.versionName;
        }
    }

}
