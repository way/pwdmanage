package cn.lastcc.pwdmanage.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 密码操作类
 * 
 * @author Added by baidongyang 2013-6-22
 * @version 13.6.22.1
 *
 */
public class PwdAction {

    private static char [] _nums = null; // 数字 权重：1
    private static final int weight_nums = 1;
    private static char [] _lowLetters = null; // 小写字母 权重：2
    private static final int weight_lowLetters = 2;
    private static char [] _uppLetters = null; // 大写字母 权重：4
    private static final int weight_uppLetters = 4;
    private static char [] _symbolChars = null; // 特殊符号 权重：8
    private static final int weight_symbolChars = 8;

    static {
        // 初始化数字元素数组
        int startAscii = 48;
        int endAscii = 57;
        _nums = new char[endAscii - startAscii + 1];
        for (int i = startAscii, index = 0; i <= endAscii; i++, index++) {
            _nums[index] = (char) i;
        }
        // 初始化小写字母元素数组
        startAscii = 97;
        endAscii = 122;
        _lowLetters = new char[endAscii - startAscii + 1];
        for (int i = startAscii, index = 0; i <= endAscii; i++, index++) {
            _lowLetters[index] = (char) i;
        }
        // 初始化大写字母元素数组
        startAscii = 65;
        endAscii = 90;
        _uppLetters = new char[endAscii - startAscii + 1];
        for (int i = startAscii, index = 0; i <= endAscii; i++, index++) {
            _uppLetters[index] = (char) i;
        }
        // 初始化特殊符号元素数组
        _symbolChars = "`~!@#$%^&*()_+-={}|[]\\:\";'<>?,./'".toCharArray();
    }

    /**
     * 生成密码
     * @param weight 权重<br/>
     * 基础权重：1 数字；2 小写字母；3 大写字母；4 特殊符号
     * @param pwdLength 密码长度
     * @return 生成的密码
     * @author Added by baidongyang 2013-6-22
     */
    public String generatePwd (int weight, int pwdLength) {
        if ((weight <= 0) || (pwdLength <= 0)) {
            return "";
        }
        List<Character> pwdElements = new ArrayList<Character>();
        // 将数字元素填入密码组合元素集合中
        if ((weight & weight_nums) > 0) {
            for (int i = 0; i < _nums.length; i++) {
                pwdElements.add(_nums[i]);
            }
        }
        // 将小写字母元素填入密码组合元素集合中
        if ((weight & weight_lowLetters) > 0) {
            for (int i = 0; i < _lowLetters.length; i++) {
                pwdElements.add(_lowLetters[i]);
            }
        }
        // 将大写字母元素填入密码组合元素集合中
        if ((weight & weight_uppLetters) > 0) {
            for (int i = 0; i < _uppLetters.length; i++) {
                pwdElements.add(_uppLetters[i]);
            }
        }
        // 将特殊符号元素填入密码组合元素集合中
        if ((weight & weight_symbolChars) > 0) {
            for (int i = 0; i < _symbolChars.length; i++) {
                pwdElements.add(_symbolChars[i]);
            }
        }
        final int pwdElementSize = pwdElements.size();
        Random random = new Random();
        StringBuilder pwdBuilder = new StringBuilder("");
        for (int i = 0; i < pwdLength; i ++) {
            pwdBuilder.append(pwdElements.get(random.nextInt(pwdElementSize)));
        }
        return pwdBuilder.toString();
    }
}
