package cn.lastcc.pwdmanage.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import android.os.Environment;

import cn.lastcc.pwdmanage.entity.IConstant;
import cn.lastcc.pwdmanage.entity.Info;

/**
 * 序列化相关工具，主要用于存取数据文件，作为本程序的持久化工具
 * 
 * @author Added by baidongyang 2013-6-23
 * @version 13.6.23.1
 *
 */
public class SerialUtil {

    private static Info _info;

    public static Info getInfo() throws StreamCorruptedException, FileNotFoundException, IOException, ClassNotFoundException {
        if (null == _info) {
            File [] files_level1 = Environment.getExternalStorageDirectory().listFiles();
            // 查找一级目录
            for (int i = 0; i < files_level1.length; i++) {
                File dir_level1 = files_level1[i];
                if (IConstant.INFO_FILE_PATH_LEVEL1.equals(dir_level1.getName())
                        && (dir_level1.isDirectory())) {
                    File [] files_level2 = dir_level1.listFiles();
                    // 查找二级目录
                    for (int j = 0; j < files_level2.length; j++) {
                        File dir_level2 = files_level2[j];
                        if (IConstant.INFO_FILE_PATH_LEVEL2.equals(dir_level2.getName())
                                && (dir_level2.isDirectory())) {
                            // 查找文件
                            File [] files_level3 = dir_level2.listFiles();
                            for (int k = 0; k < files_level3.length; k++) {
                                File dir_level3 = files_level3[k];
                                if (IConstant.INFO_FILE_NAME.equals(dir_level3.getName())
                                        && dir_level3.isFile()) {
                                    // 进行反序列化
                                    ObjectInputStream in = new ObjectInputStream(new FileInputStream(dir_level3));
                                    Object obj = in.readObject();
                                    in.close();
                                    if (obj instanceof Info) {
                                        _info = (Info) obj;
                                    } else {
                                        throw new RuntimeException("文件解析失败");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return _info;
    }

    public static void setInfo(Info info) throws FileNotFoundException, IOException {
        _info = info;
        File infoFile = new File(IConstant.INFO_FILE_PATH);
        // 删除已序列化的文件
        if (null == info) {
            infoFile.delete();
        }
        // 序列化文件保存数据
        else {
            // 如果父目录不存在则创建
            if (!infoFile.getParentFile().exists()) {
                infoFile.getParentFile().mkdirs();
            }
//            if (infoFile.exists()) {
//                infoFile.delete();
//            }
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(infoFile));
            out.writeObject(info);
            out.close();
        }
    }
}
