package cn.lastcc.pwdmanage.ui;

import java.util.Date;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cn.lastcc.pwdmanage.R;
import cn.lastcc.pwdmanage.core.AesUtil;
import cn.lastcc.pwdmanage.core.CustomApp;
import cn.lastcc.pwdmanage.core.MD5;
import cn.lastcc.pwdmanage.core.SerialUtil;
import cn.lastcc.pwdmanage.entity.Info;
import cn.lastcc.pwdmanage.entity.PwdTag;

public class UserPwdModifyActivity extends Activity {

    // 声明控件
    private EditText txtOriginPwd = null;
    private EditText txtPwd = null;
    private EditText txtConfirmPwd = null;
    private Button btnSavePwd = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("修改用户密码");
        setContentView(R.layout.activity_user_pwd_modify);
        // Show the Up button in the action bar.
//        setupActionBar();
        // 根据 ID 获得控件对象
        this.txtOriginPwd = (EditText) this.findViewById(R.id.txtOriginPwd_upm);
        this.txtPwd = (EditText) this.findViewById(R.id.txtPwd_upm);
        this.txtConfirmPwd = (EditText) this.findViewById(R.id.txtConfirmPwd_upm);
        this.btnSavePwd = (Button) this.findViewById(R.id.btnSavePwd_upm);
        Info info = null;
        try {
            info = SerialUtil.getInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 第一次使用不要求输入旧密码，所以把旧密码文本框置灰
        if (null == info) {
            this.txtOriginPwd.setEnabled(false);
            this.txtPwd.requestFocus();
        }
        this.btnSavePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Info info = null;
                try {
                    info = SerialUtil.getInfo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MD5 m = new MD5();
                // 第一次使用不要求输入旧密码
                if (null != info) {
                    final String originPwdText = txtOriginPwd.getText().toString();
                    if ("".equals(originPwdText)) {
                        Toast.makeText(v.getContext(), R.string.msg_userPwdModifyActivity_originPwdEmpty, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String originPwd = m.cell32(originPwdText);
                    if (!originPwd.equals(info.getUserPassword())) {
                        Toast.makeText(v.getContext(), R.string.msg_userPwdModifyActivity_originPwdFailed, Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    info = new Info();
                }
                final String pwdText = txtPwd.getText().toString();
                final String confirmPwdText = txtConfirmPwd.getText().toString();
                if ("".equals(pwdText)) {
                    Toast.makeText(v.getContext(), R.string.msg_userPwdModifyActivity_pwdEmpty, Toast.LENGTH_SHORT).show();
                    return;
                }
                if ("".equals(confirmPwdText)) {
                    Toast.makeText(v.getContext(), R.string.msg_userPwdModifyActivity_confirmPwdEmpty, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!pwdText.equals(confirmPwdText)) {
                    Toast.makeText(v.getContext(), R.string.msg_userPwdModifyActivity_pwdNotConfirm, Toast.LENGTH_SHORT).show();
                    return;
                }
                String _oldPwd=info.getUserPassword();
                info.setUserPassword(m.cell32(pwdText));
                String _newPwd=info.getUserPassword();
                List<PwdTag> pwdTagList=info.getPwdTagList();
	           	 for (int i = 0, j = pwdTagList.size(); i < j; i++) {
	                    PwdTag pwdTag = pwdTagList.get(i);
	                    pwdTag.setModifyDate(new Date());
	                    pwdTag.setModifyTimes(pwdTag.getModifyTimes()+1);
	                    byte [] nameResult =  AesUtil.decrypt(AesUtil.parseHexStr2Byte(pwdTag.getTagName()),_oldPwd);
	                    String _nameStr=new String(nameResult);
	                    pwdTag.setTagName( AesUtil.parseByte2HexStr(AesUtil.encrypt(_nameStr, _newPwd)));

	                   byte [] pwdResult =  AesUtil.decrypt(AesUtil.parseHexStr2Byte(pwdTag.getPwd()),_oldPwd);
	                   String  _pwdStr=new String(pwdResult);
	                    pwdTag.setPwd( AesUtil.parseByte2HexStr(AesUtil.encrypt(_pwdStr, _newPwd)));
	            }
	           	info.setPwdTagList(pwdTagList);
                CustomApp customApp = (CustomApp) UserPwdModifyActivity.this.getApplication();
                customApp.setUserPwd(_newPwd);
                info.setVersionCode(customApp.getVersionCode());
                info.setVersionName(customApp.getVersionName());
                info.setFileVersion(customApp.getVersionCode() + "");
                try {
                    SerialUtil.setInfo(info);
                    Intent intent = new Intent();
                    intent.setClass(UserPwdModifyActivity.this, LogonActivity.class);
                    startActivity(intent);
                    MainActivity.instance.finish();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(v.getContext(), R.string.msg_userPwdModifyActivity_savePwdFailed, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
