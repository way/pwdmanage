package cn.lastcc.pwdmanage.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StreamCorruptedException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import cn.lastcc.pwdmanage.R;
import cn.lastcc.pwdmanage.core.MD5;
import cn.lastcc.pwdmanage.core.SerialUtil;
import cn.lastcc.pwdmanage.entity.Info;

/**
 * 登陆窗口
 *
 * @author Added by baidongyang 2013-6-22
 * @version 13.6.22.1
 * @version 13.6.23.2
 *
 */
public class LogonActivity extends Activity {

    // 声明控件
    private EditText txtPwd = null;
    private CheckBox chkShowPwd = null;
    private Button btnLogon = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("登录");
        // 如果第一次使用本程序，则直接进入主界面
        try {
            if (null == SerialUtil.getInfo()) {
                this.startMainActivity();
                return;
            }
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
            this.startMainActivity();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            this.startMainActivity();
        } catch (IOException e) {
            e.printStackTrace();
            this.startMainActivity();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            this.startMainActivity();
        }
        setContentView(R.layout.activity_logon);
        // 通过控件 ID 获得控件对象
        this.txtPwd = (EditText) this.findViewById(R.id.txtPwd_lon);
        this.chkShowPwd = (CheckBox) this.findViewById(R.id.chkShowPwd_lon);
        this.btnLogon = (Button) this.findViewById(R.id.btnLogon_lon);
        // 给显示密码复选框添加监听事件
        this.chkShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // 如果选中则显示密码，否则隐藏密码
                if (isChecked) {
                    txtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    txtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
        // 给登陆按钮增加单击事件
        this.btnLogon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String inputPwd = txtPwd.getText().toString();
                // 校验密码不能为空
                if ("".equals(inputPwd)) {
                    Toast.makeText(v.getContext(), R.string.msg_logonActivity_pwdPrompt, Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Info info = null;
                    try {
                        info = SerialUtil.getInfo();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), R.string.msg_logonActivity_logonException, Toast.LENGTH_LONG).show();
                        return;
                    }
                    MD5 m = new MD5();
                    if (!m.cell32(inputPwd).equals(info.getUserPassword())) {
                        Toast.makeText(v.getContext(), R.string.msg_logonActivity_logonFailed, Toast.LENGTH_LONG).show();
                        return;
                    }
                    startMainActivity();
                }
            }
        });
    }

    /**
     * 启动 MainActivity 并关闭当前 Activity
     *
     * @author Added by baidongyang 2013-6-23
     */
    private void startMainActivity() {
        Intent intent = new Intent();
        intent.setClass(LogonActivity.this, MainActivity.class);
        startActivity(intent);
        // 关闭当前窗口
        LogonActivity.this.finish();
    }
}
