package cn.lastcc.pwdmanage.entity;

import android.os.Environment;

/**
 * 常量类
 * 
 * @author Added by baidongyang 2013-6-23
 * @version 13.6.23.1
 *
 */
public class IConstant {

    public final static String INFO_FILE_PATH_LEVEL1 = "cnLastcc";
    public final static String INFO_FILE_PATH_LEVEL2 = "pwdManage";
    public final static String INFO_FILE_NAME = "info.bin";
    /**
     * Info 文件保存位置
     */
    public final static String INFO_FILE_PATH = Environment.getExternalStorageDirectory() + "/" + INFO_FILE_PATH_LEVEL1 + "/" + INFO_FILE_PATH_LEVEL2 + "/" + INFO_FILE_NAME;
}
